# a Spatially Aware SEIRS Model for COVID19

to help out, change the date on the first line of `sseirs.py`, save, then run `python sseirs.py`

CTRL+C will end the simulation.  Please submit the results file by sending me a zipped results file through slack.

## more details

If you want to learn how the model works do this:

open `seirs.ipynb` your usual way.

run all the cells.

check the issues, there's work to do.

## note to those who don't like gitlab

it's better, face it. However you may feel free to move this to github, but then you're in charge of the issue and merge requests ;)
